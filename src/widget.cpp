#include<cstdlib>
#include<QKeyEvent>
#include<QDebug>
#include "widget.h"
#include "ui_widget.h"
#include "jr.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    
    #if 1
    this->setWindowFlags(windowFlags() | Qt::FramelessWindowHint);    // 去边框 
    this->setAttribute(Qt::WA_TranslucentBackground);                 // 背景透明
    this->showMaximized();                                            // 最大化 

    
    for(int i = 0; i < 50; ++i) 
    {
        srand(i * 10); 
        Jr *jr = new Jr(this); 
        jr->move(rand() % this->width(), rand() % this->height()); 
        jr->show();      // 背景透明之后需要重新show一下 
    }
    #endif 
}

Widget::~Widget()
{
    delete ui;
}


void Widget::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return) this->close();
}


void Widget::get_score()
{
    this->score++; 
    this->ui->lcdNumber->display(this->score); 
}