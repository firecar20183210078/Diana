#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    void get_score();
    ~Widget();

protected:
    void keyPressEvent(QKeyEvent *event); 

private:
    Ui::Widget *ui;
    int score = 0; 
};

#endif // WIDGET_H
