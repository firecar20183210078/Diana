#ifndef JR_H
#define JR_H

#include <QWidget>
//#include <QPixmap>

class Jr : public QWidget
{
    Q_OBJECT
public:
    explicit Jr(QWidget *parent = nullptr);

protected:
    void paintEvent(QPaintEvent *ev); 
    void timerEvent(QTimerEvent *ev); 
    void mousePressEvent(QMouseEvent *event);
    // void mouseMoveEvent(QMouseEvent *event); 
    
signals:
    void hit(); 
private:
    int index = 0; 
    QPixmap *jr;
    QPoint offset;
    int rand_num = 0;
};

#endif // JR_H